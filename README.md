# the library of beautiful things
This repository contains code, documentation, and demo's for a compelling VR & MR experience compatible with the Meta Quest 2 and 3.

- MIT Reality Hack 2024
- Future Constructors: Health & Wellbeing

## Team 87

Anne-Isabell de Bokay
Louise Edwards
Tamar Haber-Schaim
Juwon Lee
Hammad Usmani

## Requirements

- Unity Hub (https://unity.com/download)
- Unity 2022.3.16 (unityhub://2022.3.16f1/d2c21f0ef2f1)
- Quest Development Mode (https://developer.oculus.com/documentation/native/android/mobile-device-setup/)

## Quickstart

1. Run `git clone https://codeberg.org/reality-hack-2024/TABLE_87.git`
2. Open Unity Hub and select the project contained in the `unity` directory
3. Connect Quest headset to computer with a data cable
4. Configure build settings and then `Build and Run`

### Hardware Required

Either,

- Quest 2 or Quest 3
- Quest Link or Quest compatible USB-C data cable

### Software Dependencies

- Linux, Mac, and Windows.
- Unity 2022.3.16

## Awknowledgements
Thank you to all of our wonderful mentors in helping us bring our vision to life.

## References
Please visit this issue page with references. I
https://codeberg.org/reality-hack-2024/TABLE_87/issues/12